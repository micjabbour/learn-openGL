#include <QtGui>

class MyWindow : public QOpenGLWindow
{
public:
    MyWindow();
    ~MyWindow();

    void initializeGL() override;
    void paintGL() override;

private:
    GLuint m_posAttr;
    GLuint triangleVao;
    GLuint triangleVbo;

    QOpenGLShaderProgram *m_program;
};

MyWindow::MyWindow()
    : triangleVao(0), triangleVbo(0), m_program(0)
{
}

MyWindow::~MyWindow(){
    QOpenGLExtraFunctions* f = context()->extraFunctions();
    f->glDeleteVertexArrays(1, &triangleVao);
    f->glDeleteBuffers(1, &triangleVbo);
}

static const char *vertexShaderSource =
        R"GLSL(
        in vec2 aPos;
        void main()
        {
            gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0);
        })GLSL";

static const char *fragmentShaderSource =
    R"GLSL(
        out vec4 FragColor;
        void main()
        {
            FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
        }
    )GLSL";

void MyWindow::initializeGL()
{
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    m_program->link();
    m_posAttr = m_program->attributeLocation("aPos");

    GLfloat vertices[] = {
        0.5f, -0.5f,
        -0.5f, -0.5f,
        0.0f, 0.366f
    };

    QOpenGLExtraFunctions* f = context()->extraFunctions();

    f->glGenVertexArrays(1, &triangleVao);
    f->glGenBuffers(1, &triangleVbo);

    f->glBindVertexArray(triangleVao);
    f->glBindBuffer(GL_ARRAY_BUFFER, triangleVbo);
    f->glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    f->glVertexAttribPointer(m_posAttr, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), static_cast<void*>(0));
    f->glEnableVertexAttribArray(m_posAttr);
    f->glBindVertexArray(0);
}

void MyWindow::paintGL()
{
    QOpenGLExtraFunctions* f = context()->extraFunctions();
    const qreal retinaScale = devicePixelRatio();
    f->glViewport(0, 0, width() * retinaScale, height() * retinaScale);

    f->glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    f->glClear(GL_COLOR_BUFFER_BIT);

    m_program->bind();
    f->glBindVertexArray(triangleVao);
    f->glDrawArrays(GL_TRIANGLES, 0, 3);

    m_program->release();
}


int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    MyWindow window;
    window.resize(480, 480);
    window.show();

    return app.exec();
}
