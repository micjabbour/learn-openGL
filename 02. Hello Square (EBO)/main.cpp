#include <QtGui>

class MyWindow : public QOpenGLWindow
{
public:
    MyWindow();
    ~MyWindow();

    void initializeGL() override;
    void paintGL() override;

private:
    GLuint m_posAttr;
    GLuint rectVao;
    GLuint rectVbo;
    GLuint rectEbo;

    QOpenGLShaderProgram *m_program;
};

MyWindow::MyWindow()
    : rectVao(0), rectVbo(0), rectEbo(0), m_program(0)
{
}

MyWindow::~MyWindow(){
    QOpenGLExtraFunctions* f = context()->extraFunctions();
    f->glDeleteVertexArrays(1, &rectVao);
    f->glDeleteBuffers(1, &rectVbo);
    f->glDeleteBuffers(1, &rectEbo);
}

static const char *vertexShaderSource =
        R"GLSL(
        in vec3 aPos;
        void main()
        {
        gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
        })GLSL";

static const char *fragmentShaderSource =
        R"GLSL(
        out vec4 FragColor;
        void main()
        {
        FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
        }
        )GLSL";

void MyWindow::initializeGL()
{
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    m_program->link();
    m_posAttr = m_program->attributeLocation("aPos");

    float vertices[] = {
        0.5f,  0.5f, 0.0f,  // top right
        0.5f, -0.5f, 0.0f,  // bottom right
        -0.5f, -0.5f, 0.0f,  // bottom left
        -0.5f,  0.5f, 0.0f   // top left
    };

    unsigned int indices[] = {
        0, 1, 3, //first triangle
        1, 2, 3  //second triangle
    };

    QOpenGLExtraFunctions* f = context()->extraFunctions();

    f->glGenVertexArrays(1, &rectVao);
    f->glGenBuffers(1, &rectVbo);
    f->glGenBuffers(1, &rectEbo);

    f->glBindVertexArray(rectVao);
    f->glBindBuffer(GL_ARRAY_BUFFER, rectVbo);
    f->glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, rectEbo);
    f->glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    f->glVertexAttribPointer(m_posAttr, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), static_cast<void*>(0));
    f->glEnableVertexAttribArray(m_posAttr);
    f->glBindVertexArray(0);
}

void MyWindow::paintGL()
{
    QOpenGLExtraFunctions* f = context()->extraFunctions();
    const qreal retinaScale = devicePixelRatio();
    f->glViewport(0, 0, width() * retinaScale, height() * retinaScale);

    f->glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    f->glClear(GL_COLOR_BUFFER_BIT);

    m_program->bind();
    f->glBindVertexArray(rectVao);
    f->glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    m_program->release();
}


int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    MyWindow window;
    window.resize(480, 480);
    window.show();

    return app.exec();
}
